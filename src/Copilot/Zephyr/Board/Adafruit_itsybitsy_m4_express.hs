-- | Programming the adafruit_itsybitsy_m4_express board with Copilot.

{-# LANGUAGE DataKinds #-}

module Copilot.Zephyr.Board.Adafruit_itsybitsy_m4_express (
	module Copilot.Zephyr
	, sw0
	, led0
	-- * Pins
	, pin0
	, pin1
	, pin2
	, pin3
	, pin4
	, pin7
	, pin9
	, pin10
	, pin11
	, pin12
	, pin13
	, sck
	, mosi
	, miso
) where

import Copilot.Zephyr
import Copilot.Zephyr.Internals
import Copilot.Zephyr.Board.Generic (sw0, led0)

-- TODO: a0, a1, a2-a5
-- TODO: pin5 is output-only, not input, intended for PWM, but may be able
-- to do digital output too. Current types do not allow output-only.
-- TODO: several other pins can be analog input
-- TODO: many pins support pwm, including led0
-- TODO: DotStar led (pin #6 and #8)

-- The GPIO addresses were found by reading the schematic at
-- https://learn.adafruit.com/introducing-adafruit-itsybitsy-m4/downloads
-- eg, pin0 is labeled as "D0" and connects to "PA16".

pin0, pin1, pin2, pin3, pin4, pin7, pin9, pin10 :: Pin '[ 'DigitalIO ]
pin11, pin12, pin13, sck, mosi, miso :: Pin '[ 'DigitalIO ]

-- | RX
pin0 = Pin (Zephyr (GPIOAlias "gpio_pin_0") (porta 16)) 
-- | TX
pin1 = Pin (Zephyr (GPIOAlias "gpio_pin_1") (porta 17)) 
pin2 = Pin (Zephyr (GPIOAlias "gpio_pin_2") (porta 7))
pin3 = Pin (Zephyr (GPIOAlias "gpio_pin_3") (portb 22))
pin4 = Pin (Zephyr (GPIOAlias "gpio_pin_4") (porta 15))
pin7 = Pin (Zephyr (GPIOAlias "gpio_pin_7") (porta 18))
pin9 = Pin (Zephyr (GPIOAlias "gpio_pin_9") (porta 19))
pin10 = Pin (Zephyr (GPIOAlias "gpio_pin_10") (porta 20))
pin11 = Pin (Zephyr (GPIOAlias "gpio_pin_11") (porta 21))
pin12 = Pin (Zephyr (GPIOAlias "gpio_pin_12") (porta 23))
-- | same as led0
pin13 = led0 
-- | SPI SCK
sck = Pin (Zephyr (GPIOAlias "gpio_pin_sck") (porta 1))
-- | SPI MOSI
mosi = Pin (Zephyr (GPIOAlias "gpio_pin_mosi") (porta 0))
-- | SPI MISO
miso = Pin (Zephyr (GPIOAlias "gpio_pin_miso") (portb 23))

porta :: Int -> GPIOAddress
porta n = GPIOAddress ("porta " <> show n)

portb :: Int -> GPIOAddress
portb n = GPIOAddress ("portb " <> show n)
