-- | This module exposes inputs and outputs that Zephyr provides
-- for many boards, such as led0 and sw0.
--
-- When used with a board that does not support a particular input or
-- output, compilation of the generated C code will fail with an error.

{-# LANGUAGE RebindableSyntax #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Copilot.Zephyr.Board.Generic
(
	led0,
	sw0,
	module X,
) where

import Copilot.Zephyr as X
import Copilot.Zephyr.Internals

-- | An on-board LED, connected to a GPIO pin.
--
-- Many boards include such a LED, which is referred to as the "User LED"
-- in Zephyr's documentation.
led0 :: Pin '[ 'DigitalIO ]
led0 = Pin (Zephyr (GPIOAlias "led0") GPIOAddressBuiltIn)

-- | A push button, connected to a GPIO pin.
-- 
-- Many boards include such a button, which is referred to as the "User Button"
-- in Zephyr's documentation.
sw0 :: Pin '[ 'DigitalIO ]
sw0 = Pin (Zephyr (GPIOAlias "sw0") GPIOAddressBuiltIn)
