-- | Programming the adafruit_feather_m0_basic_proto board with Copilot.

{-# LANGUAGE DataKinds #-}

module Copilot.Zephyr.Board.Adafruit_feather_m0_basic_proto (
	module Copilot.Zephyr
	, sw0
	, led0
	-- * Pins
	, pin0
	, pin1
	, pin5
	, pin6
	, pin9
	, pin10
	, pin11
	, pin12
	, pin13
	, pin20
	, pin21
) where

import Copilot.Zephyr
import Copilot.Zephyr.Internals
import Copilot.Zephyr.Board.Generic (sw0, led0)

-- TODO: a0, a1-a5
-- TODO: several other pins can be analog input
-- TODO: several pins support pwm

-- The GPIO addresses were found by reading the schematic at
-- https://learn.adafruit.com/adafruit-feather-m0-basic-proto/downloads
-- eg, pin13 is labeled as "D13" and connects to "PA17".

pin0, pin1, pin5, pin6, pin9, pin10, pin11 :: Pin '[ 'DigitalIO ]
pin12, pin13, pin20, pin21 :: Pin '[ 'DigitalIO ]

pin0 = Pin (Zephyr (GPIOAlias "gpio_pin_0") (porta 11))
pin1 = Pin (Zephyr (GPIOAlias "gpio_pin_1") (porta 10))
pin5 = Pin (Zephyr (GPIOAlias "gpio_pin_5") (porta 15))
pin6 = Pin (Zephyr (GPIOAlias "gpio_pin_6") (porta 20))
pin9 = Pin (Zephyr (GPIOAlias "gpio_pin_9") (porta 7))
pin10 = Pin (Zephyr (GPIOAlias "gpio_pin_10") (porta 18))
pin11 = Pin (Zephyr (GPIOAlias "gpio_pin_11") (porta 16))
pin12 = Pin (Zephyr (GPIOAlias "gpio_pin_12") (porta 19))
-- | same as led0
pin13 = led0
-- | SDA
pin20 = Pin (Zephyr (GPIOAlias "gpio_pin_20") (porta 22))
-- | SCL
pin21 = Pin (Zephyr (GPIOAlias "gpio_pin_21") (porta 23))

porta :: Int -> GPIOAddress
porta n = GPIOAddress ("porta " <> show n)
