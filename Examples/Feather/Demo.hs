{-# LANGUAGE RebindableSyntax #-}

module Examples.Feather.Demo where

import Copilot.Zephyr.Board.Adafruit_feather_m0_basic_proto

main :: IO ()
main = zephyr $ do
	x <- input' pin9 [False, False, False, True, True]
	led0 =: not x

	pin12 =: true
	pin11 =: false
	pin10 =: blinking
	pin6 =: frequency 4
	pin5 =: frequency 8

	delay =: MilliSeconds (constant 100)
