{-# LANGUAGE RebindableSyntax #-}

module Examples.Blink.Demo where

import Copilot.Zephyr.Board.Generic

main :: IO ()
main = zephyr $ do
	led0 =: blinking
	delay =: MilliSeconds (constant 100)
