-- The examples also constitute the test suite: Make sure they all compile.

import Control.Monad
import Control.Exception
import System.Directory
import System.IO
import System.IO.Temp

import qualified Examples.Blink.Demo
import qualified Examples.Button.Demo
import qualified Examples.Feather.Demo

data TestCase = TestCase
	{ desc :: String
	, builder :: IO ()
	, compileC :: Bool
	}

tests :: [TestCase]
tests =
	[ TestCase "Blink" Examples.Blink.Demo.main True
	, TestCase "Button" Examples.Button.Demo.main True
	, TestCase "Feather" Examples.Feather.Demo.main True
	]

main :: IO ()
main = forM_ tests (runtest False)

runtest :: Bool -> TestCase -> IO ()
runtest canccompile t = withSystemTempDirectory "arduino-copilot-test" $ \tmpdir ->
	bracket (setup tmpdir) cleanup (const copilottest)
  where
	setup tmpdir = do
		setCurrentDirectory tmpdir
	cleanup _ = return ()
	
	copilottest = do
		putStr (desc t ++ " copilot ")
		hFlush stdout
		r <- try' (builder t)
		case r of
			Right () -> do
				putStrLn "OK"
				hFlush stdout
				ccompiletest
			Left ex ->
				 putStrLn ("FAILED: " ++ show ex)

	ccompiletest = when (canccompile && compileC t) $ do
		putStr (desc t ++ " C compilation ")
		error "compilation not implemented"

	try' :: IO () -> IO (Either SomeException ())
	try' = try
